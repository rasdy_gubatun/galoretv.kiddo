require 'spec_helper'

describe Post do
  
  describe "slug" do
    it "should autogenerate slug" do
      @post = Post.new
      @post.name = "This is a blog entry"
      @post.save
      expect(@post.slug).to eq('this-is-a-blog-entry')
      
      @post2 = Post.new
      @post.name = "Rasdy doodle is badoodle"
      @post.slug = "astig"
      @post.save
      expect(@post.slug).to eq('rasdy-doodle-is-badoodle')  
    end
    
    it "should be able to handle apostrophe on slug generation" do
      @post = Post.new
      @post.name = "Rasdy's New Year's Resolution"
      @post.save
      expect(@post.slug).to eq('rasdys-new-years-resolution')
    end
    
  end
  
  
end
