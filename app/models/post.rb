class Post
  include Mongoid::Document
  field :name
  field :slug
  
  before_save :generate_slug
  
  def generate_slug
    self.slug = self.name.downcase.gsub(/\s/, '-').gsub(/[']/, '')
  end
  
end
